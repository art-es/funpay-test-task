<?php

const EMULATOR_API_URL = 'https://funpay.ru/yandex/emulator';

/**
 * Get payment info
 * @param int $receiver
 * @param int $sum
 * @return array
 * @throws RuntimeException
 */
function getPaymentInfo(int $receiver, int $sum): array {
    $context = stream_context_create([
        'http' => [
            'method' => 'POST',
            'header' => "x-requested-with: XMLHttpRequest\r\n".
                "Content-type: application/x-www-form-urlencoded\r\n",
            'content' => http_build_query([
                'receiver' => $receiver,
                'sum' => $sum,
            ]),
        ],
    ]);
    $resp = file_get_contents(EMULATOR_API_URL, false, $context);
    if ($resp === false) {
        throw new RuntimeException('Error in request!', 1);
    }

    $fields = explode('<br />', $resp);

    try {
        $code = preg_replace('/(Пароль\: )(\d+)/ui', '$2', $fields[0]);
        $sum = trim(preg_replace('/(Спишется )([\dА-яЁё\.]+)/ui', '$2', $fields[1]));
        $receiver = trim(preg_replace('/(Перевод на счет )([\d]+)/ui', '$2', $fields[2]));
    } catch (RuntimeException $exception) {
        throw new RuntimeException('Error during parsing!', 1);
    }

    return compact('code', 'sum', 'receiver');
}

$fakeReceiver = 41001569362890;
$fakeSum = 1000;

$info = getPaymentInfo($fakeReceiver, $fakeSum);
var_dump($info);
